﻿using System;

namespace MyADO.DAL.Connected.Models
{
   public class AfterJoinModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int TodoItemsId { get; set; }
        public int CategoryId { get; set; }
        public int TodoId { get; set; }
        public string TaskDescription { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int CategId { get; set; }
        public string CategoryName { get; set; }
    }
}
