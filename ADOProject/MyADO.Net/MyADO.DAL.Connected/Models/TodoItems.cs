﻿using System;

namespace MyADO.DAL.Connected.Repositories
{
    public class TodoItems
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
