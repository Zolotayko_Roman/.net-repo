﻿using System.Configuration;
using System.Data.SqlClient;

namespace MyADO.DAL.Connected.Repositories
{
    public class BaseRepository
    {
        protected const string connectionStringName = "DBConnection";

        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[connectionStringName].ToString();
            }
        }

        public void ExecuteNonQuery(string sqlCommand)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlCommand, connection);
                int number = command.ExecuteNonQuery();
            }
        }

    }
}
