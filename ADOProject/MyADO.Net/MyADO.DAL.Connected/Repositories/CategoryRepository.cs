﻿using MyADO.DAL.Connected.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace MyADO.DAL.Connected.Repositories
{
    public class CategoryRepository:BaseRepository, IRepository<Category>
    {
        public void Add(Category example)
        {
            string insertCategorysSql = $@"insert into Catgory (id,CatagoryName) VALUES('{example.Id}',
                '{example.Name}'
                )";
            ExecuteNonQuery(insertCategorysSql);
        }

        public void Delete(Category example, int id)
        {
            string deleteCategorySql = $@"delete  from Category where id='{example.Id}'";
            ExecuteNonQuery(deleteCategorySql);
        }

        public void Update(Category example, int id)
        {
            string deleteCategorySql = $@"update Category set CategoryName='{example.Name}'
                                                   where id='{id}'";
            ExecuteNonQuery(deleteCategorySql);
        }

        public IEnumerable<Category> Read()
        {
            string readUserSql = "SELECT * FROM Category";
            var result = new List<Category>();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(readUserSql, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result.Add(new Category
                        {

                            Id = Convert.ToInt32(reader[nameof(Category.Id)].ToString()),
                            Name = reader[nameof(Category.Name)].ToString().Trim()
                        });
                    }
                
                    reader.Close();
                }
                return result;
            }

        }
    }
}
