﻿using MyADO.DAL.Connected.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace MyADO.DAL.Connected.Repositories
{
    public class TodoItemsRepository:BaseRepository, IRepository<TodoItems>
    {
        public void Add(TodoItems example)
        {
            string insertTodoSql = $@"insert into TodoItems (id,TaskDescription,CreatedAt) VALUES('{example.Id}',
                '{example.Description}',
                '{example.CreatedAt}'
                )";
            ExecuteNonQuery(insertTodoSql);
        }

        public void Delete(TodoItems example, int id)
        {
            string deleteTodoSql = $@"delete  from TodoItems where id='{example.Id}'";
            ExecuteNonQuery(deleteTodoSql);
        }

        public void Update(TodoItems example, int id)
        {
            string deleteTodoSql = $@"update TodoItems set TaskDescription='{example.Description}'
                                                       set CreatedAt='{example.CreatedAt}' 
                                                   where id='{id}'";
            ExecuteNonQuery(deleteTodoSql);
        }

        public IEnumerable<TodoItems> Read()
        {
            string readUserSql = "SELECT * FROM TodoItems";
            var result = new List<TodoItems>();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(readUserSql, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        result.Add(new TodoItems
                        {

                            Id = Convert.ToInt32(reader[nameof(TodoItems.Id)].ToString()),
                            Description = reader[nameof(TodoItems.Description)].ToString().Trim(),
                            CreatedAt = reader.GetDateTime(2)
                        });
                    }
                    reader.Close();
                }
                return result;
            }

        }
    }
}
