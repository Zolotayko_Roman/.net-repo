﻿using MyADO.DAL.Connected.Interface;
using MyADO.DAL.Connected.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace MyADO.DAL.Connected.Repositories
{
    public class UserRepository :BaseRepository, IRepository<Users>
    {
        public void Add(Users example)
        {
            string insertUsersSql = $@"insert into Users (FirstName,LastName,TodoItemsId,CategoryId) VALUES('{example.FirstName}',
                '{example.LastName}',
                '{example.TodoItemsId}',
                '{example.CategoryId}'
                )";
            ExecuteNonQuery(insertUsersSql);
        }

        public void Delete(Users example, int id)
        {
            string deleteUserSql = $@"delete  from Users where id='{example.Id}'";
            ExecuteNonQuery(deleteUserSql);
        }

        public void Update(Users example, int id)
        {
            string deleteUserSql = $@"update Users set FirstName='{example.FirstName}'
                                                   where id='{id}'";
            ExecuteNonQuery(deleteUserSql);
        }

        public IEnumerable<Users> Read()
        {
            string readUserSql = "SELECT * FROM Users";
            var result = new List<Users>();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(readUserSql, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result.Add(new Users
                        {

                            Id = Convert.ToInt32(reader[nameof(Users.Id)].ToString()),
                            FirstName = reader[nameof(Users.FirstName)].ToString().Trim(),
                            LastName = reader[nameof(Users.LastName)].ToString().Trim(),
                            TodoItemsId = Convert.ToInt32(reader[nameof(Users.TodoItemsId)].ToString()),
                            CategoryId = Convert.ToInt32(reader[nameof(Users.CategoryId)].ToString())
                        });
                    }
                    reader.Close();
                }
                return result;
            }
        }
        public void JoinDependentTables()
        {
            var result = new List<AfterJoinModel>();
            string JoinSql = $@"select *
                               from Users
                               join TodoItems on Users.TodoItemsId=TodoItems.id
                               join Category on Users.CategoryId=Category.id";
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(JoinSql, connection);
                    SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result.Add(new AfterJoinModel
                        {
                            Id = Convert.ToInt32(reader[nameof(Users.Id)].ToString()),
                            FirstName = reader[nameof(Users.FirstName)].ToString().Trim(),
                            LastName = reader[nameof(Users.LastName)].ToString().Trim(),
                            TodoItemsId = Convert.ToInt32(reader[nameof(Users.TodoItemsId)].ToString()),
                            CategoryId = Convert.ToInt32(reader[nameof(Users.CategoryId)].ToString()),
                            TodoId = Convert.ToInt32(reader[nameof(TodoItems.Id)].ToString()),
                            TaskDescription = reader[nameof(TodoItems.Description)].ToString().Trim(),
                            CreatedAt = reader.GetDateTime(7),
                            CategId = Convert.ToInt32(reader[nameof(Category.Id)].ToString()),
                            CategoryName = reader.GetString(9)
                        });
                    }
                }
                 reader.Close();
                }
                }
        }
}
