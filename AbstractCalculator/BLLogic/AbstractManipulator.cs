﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLogic
{
   public abstract class AbstractManipulator
    {
        public abstract double Add(double x, double y);
        public abstract double Substruct(double x, double y);
        public abstract double Multiply(double x, double y);
        public abstract double Divide(double x, double y);
        public abstract ComplexNumbers Add(ComplexNumbers x, ComplexNumbers y);
        public abstract ComplexNumbers Substruct(ComplexNumbers x, ComplexNumbers y);
        public abstract ComplexNumbers Multiply(ComplexNumbers x, ComplexNumbers y);
        public abstract ComplexNumbers Divide(ComplexNumbers x, ComplexNumbers y);
    }
}
