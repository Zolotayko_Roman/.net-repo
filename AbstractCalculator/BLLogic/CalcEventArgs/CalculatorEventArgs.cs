﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLogic.CalcEventArgs
{
   public class CalculatorEventArgs<T,X>:EventArgs
    {
        public string message;
        public bool isValid;
        public T FirstElement;
        public X SecondElement;
        public CalculatorEventArgs(string msg)
        {
            message = msg;
            isValid = true;
        }
        public CalculatorEventArgs(T firstItem, X secondItem)
        {
            isValid = true;
            FirstElement = firstItem;
            SecondElement = secondItem;
        }
        public CalculatorEventArgs()
        {

        }
    }
}
