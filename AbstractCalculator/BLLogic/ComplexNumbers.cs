﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLogic
{
   public class ComplexNumbers
    {
        public double real, imaginary;
        public ComplexNumbers()
        {

        }
        public ComplexNumbers(double real, double imaginary)
        {
            this.real = real;
            this.imaginary = imaginary;
        }
        public ComplexNumbers Add(ComplexNumbers c1, ComplexNumbers c2)
        {
            return new ComplexNumbers(c1.real + c2.real, c1.imaginary + c2.imaginary);
        }
        public ComplexNumbers Substruct(ComplexNumbers c1, ComplexNumbers c2)
        {

            return new ComplexNumbers(c1.real - c2.real, c1.imaginary - c2.imaginary);
        }
        public ComplexNumbers Multiply(ComplexNumbers c1, ComplexNumbers c2)
        {

            return new ComplexNumbers(c1.real * c2.real, c1.imaginary * c2.imaginary);
        }
        public ComplexNumbers Divide(ComplexNumbers c1, ComplexNumbers c2)
        {

            return new ComplexNumbers(c1.real / c2.real, c1.imaginary / c2.imaginary);
        }
        public override string ToString()
        {
            return string.Format($"The result of this operation() is: {real} + {imaginary}i");
        }
    }
}
