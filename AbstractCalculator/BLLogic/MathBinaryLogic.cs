﻿using BLLogic.CalcEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLogic
{
    public class MathBinaryLogic : AbstractManipulator
    {
        ComplexNumbers numbers = new ComplexNumbers();
        public event EventHandler<CalculatorEventArgs<double, double>> BeforeOperation;
        public void OnBeforeAdded(CalculatorEventArgs<double,double> e) { BeforeOperation?.Invoke(this, e); }
        public event EventHandler<CalculatorEventArgs<double, double>> AfterOperation;
        public override double Add(double x, double y)
        {
            var isValid = new CalculatorEventArgs<double, double>(x,y);
            BeforeOperation(this,isValid);
            if (isValid.isValid)
            {
                return x + y;
            }
            else
            {
                AfterOperation(this, new CalculatorEventArgs<double, double>("You entering incorrect numbers"));
            }
            return 0;
        }
        public override double Substruct(double x, double y)
        {
            var isValid = new CalculatorEventArgs<double, double>(x, y);
            BeforeOperation(this, isValid);
            if (isValid.isValid)
            {
                return x - y;
            }
            else
            {
                AfterOperation(this, new CalculatorEventArgs<double, double>("You entering incorrect numbers"));
            }
            return 0;
        }
        public override double Multiply(double x, double y)
        {
            var isValid = new CalculatorEventArgs<double, double>(x, y);
            BeforeOperation(this, isValid);
            if (isValid.isValid)
            {
                return x * y;
            }
            else
            {
                AfterOperation(this, new CalculatorEventArgs<double, double>("You entering incorrect numbers"));
            }
            return 0;
        }
        public override double Divide(double x, double y)
        {
            var isValid = new CalculatorEventArgs<double, double>(x, y);
            BeforeOperation(this, isValid);
            if (isValid.isValid)
            {
                return x / y;
            }
            else
            {
                AfterOperation(this, new CalculatorEventArgs<double, double>("You entering incorrect numbers"));
            }
            return 0;
        }
        public override ComplexNumbers Add(ComplexNumbers x, ComplexNumbers y)
        {
            return numbers.Add(x, y);
        }
        public override ComplexNumbers Substruct(ComplexNumbers x, ComplexNumbers y)
        {
            return numbers.Substruct(x, y);
        }
        public override ComplexNumbers Multiply(ComplexNumbers x, ComplexNumbers y)
        {
            return numbers.Multiply(x, y);
        }
        public override ComplexNumbers Divide(ComplexNumbers x, ComplexNumbers y)
        {
            return numbers.Divide(x, y);
        }

    }
}

