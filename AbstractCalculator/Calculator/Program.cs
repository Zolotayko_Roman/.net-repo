﻿using BLLogic;
using Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Controller NewControll = new Controller();
            NewControll.SwitchBetweenCalcs();
            Console.ReadKey();
        }
    }
}
