﻿using MyADO.DAL.Connected.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADO.DAL.Connected.Context
{
   public class DBConnection:DbContext
    {
        public DBConnection():base("DBConnection")
        {
        }
        public DbSet<Users> Users { get; set; }
        public DbSet<TodoItems> ToDoItems { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
