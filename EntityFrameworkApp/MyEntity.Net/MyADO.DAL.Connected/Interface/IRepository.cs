﻿using System.Collections.Generic;

namespace MyADO.DAL.Connected.Interface
{
    public interface IRepository<T> where T:class
    {
        void Add(T example);
        void Delete(int id);
        void Update(T example, int id);
        IEnumerable<T> Read();
    }
}
