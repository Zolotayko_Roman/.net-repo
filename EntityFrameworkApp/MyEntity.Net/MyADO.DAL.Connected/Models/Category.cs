﻿namespace MyADO.DAL.Connected.Repositories
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
