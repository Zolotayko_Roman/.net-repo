﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyADO.DAL.Connected.Models
{
    public class ShowDataModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Todoitems { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Category { get; set; }
    }
}
