﻿namespace MyADO.DAL.Connected.Repositories
{
    public class Users
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int TodoItemsId { get; set; }
        public int CategoryId { get; set; } 
    }
}
