﻿using MyADO.DAL.Connected.Context;
using MyADO.DAL.Connected.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyADO.DAL.Connected.Repositories
{
    public class CategoryRepository:IRepository<Category>
    {
        public void Add(Category example)
        {
            using (var context = new DBConnection())
            {
                if (example != null)
                {
                    context.Categories.Add(example);
                }

                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using(var context = new DBConnection())
            {
                var category = context.Categories.FirstOrDefault(o => o.Id.Equals(id));
                if (category != null)
                {
                    context.Categories.Remove(category);
                }

                context.SaveChanges();
            }
        }

        public void Update(Category example, int id)
        {
            using (var context = new DBConnection())
            {
                var updateModel = context.Categories.SingleOrDefault(o => example.Id.Equals(id));
                if (updateModel !=null)
                {
                    updateModel.Name = example.Name;
                }

                context.SaveChanges();
            }
        }
        public IEnumerable<Category> Read()
        {
            var MyList = new List<Category>();
            using(var context = new DBConnection())
            {
                 MyList = context.Categories.ToList();
            }

            return MyList;
        }
    }
}
