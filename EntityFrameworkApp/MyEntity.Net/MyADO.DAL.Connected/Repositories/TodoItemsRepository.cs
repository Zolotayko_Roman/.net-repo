﻿using MyADO.DAL.Connected.Context;
using MyADO.DAL.Connected.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyADO.DAL.Connected.Repositories
{
    public class TodoItemsRepository:IRepository<TodoItems>
    {
        public void Add(TodoItems example)
        {
            using (var context = new DBConnection())
            {
                if (example != null)
                {
                    context.ToDoItems.Add(example);
                }

                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new DBConnection())
            {
                var item = context.ToDoItems.FirstOrDefault(o => o.Id.Equals(id));
                if (item != null)
                {
                    context.ToDoItems.Remove(item);
                }

                context.SaveChanges();
            }
        }

        public void Update(TodoItems example, int id)
        {
            using (var context = new DBConnection())
            {
                var updateModel = context.ToDoItems.SingleOrDefault(o => example.Id.Equals(id));
                if (updateModel != null)
                {
                    updateModel.Description = example.Description;
                    updateModel.CreatedAt = example.CreatedAt;
                }

                context.SaveChanges();
            }
        }
        public IEnumerable<TodoItems> Read()
        {
            var MyList = new List<TodoItems>();
            using (var context = new DBConnection())
            {
                MyList = context.ToDoItems.ToList();
            }

            return MyList;
        }
    }
}
