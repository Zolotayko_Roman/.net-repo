﻿using MyADO.DAL.Connected.Context;
using MyADO.DAL.Connected.Interface;
using MyADO.DAL.Connected.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyADO.DAL.Connected.Repositories
{
    public class UserRepository :IRepository<Users>
    {
        public void Add(Users example)
        {
            using (var context = new DBConnection())
            {
                if (example != null)
                {
                    context.Users.Add(example);
                }

                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new DBConnection())
            {
                var user = context.Users.FirstOrDefault(o => o.Id.Equals(id));
                if (user != null)
                {
                    context.Users.Remove(user);
                }

                context.SaveChanges();
            }
        }

        public void Update(Users example, int id)
        {
            using (var context = new DBConnection())
            {
                var updateModel = context.Users.SingleOrDefault(o => example.Id.Equals(id));
                if (updateModel != null)
                {
                    updateModel.FirstName = example.FirstName;
                    updateModel.LastName = example.LastName;
                    updateModel.TodoItemsId = example.TodoItemsId;
                    updateModel.CategoryId = example.CategoryId;
                }

                context.SaveChanges();
            }
        }
        public IEnumerable<Users> Read()
        {
            var MyList = new List<Users>();
            using (var context = new DBConnection())
            {
                MyList = context.Users.ToList();
            }

            return MyList;
        }
        public IEnumerable<ShowDataModel> GetUser()
        {
            using(var context = new DBConnection())
            {
                var usersList = (from u in context.Users
                            join td in context.ToDoItems
                            on u.TodoItemsId equals td.Id
                            join ct in context.Categories
                            on u.CategoryId equals ct.Id
                            select new ShowDataModel
                            {
                                Id = u.Id,
                                FirstName = u.FirstName,
                                LastName = u.LastName,
                                Todoitems = td.Description,
                                CreatedAt = td.CreatedAt,
                                Category = ct.Name
                            }).ToList();        
                return usersList;
            }
        }
    }
}
