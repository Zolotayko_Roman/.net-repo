﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLogic
{
    public interface IOperation
    {
        double Add(double x, double y);
        double Substruct(double x, double y);
        double Multiply(double x, double y);
        double Divide(double x, double y);
        ComplexNumbers Add(ComplexNumbers x, ComplexNumbers y);
        ComplexNumbers Substruct(ComplexNumbers x, ComplexNumbers y);
        ComplexNumbers Multiply(ComplexNumbers x, ComplexNumbers y);
        ComplexNumbers Divide(ComplexNumbers x, ComplexNumbers y);
    }
}
