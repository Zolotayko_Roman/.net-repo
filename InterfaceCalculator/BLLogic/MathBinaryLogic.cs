﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLogic
{
    public class MathBinaryLogic : IOperation
    {
        ComplexNumbers numbers = new ComplexNumbers();
        public double Add(double x, double y)
        {
            return x + y;
        }
        public double Substruct(double x, double y)
        {
            return x - y;
        }
        public double Multiply(double x, double y)
        {
            return x * y;
        }
        public double Divide(double x, double y)
        {
            return x / y;
        }
        public ComplexNumbers Add(ComplexNumbers x, ComplexNumbers y)
        {
            return numbers.Add(x, y);
        }
        public ComplexNumbers Substruct(ComplexNumbers x, ComplexNumbers y)
        {
            return numbers.Substruct(x, y);
        }
        public ComplexNumbers Multiply(ComplexNumbers x, ComplexNumbers y)
        {
            return numbers.Multiply(x, y);
        }
        public ComplexNumbers Divide(ComplexNumbers x, ComplexNumbers y)
        {
            return numbers.Divide(x, y);
        }

    }
}

