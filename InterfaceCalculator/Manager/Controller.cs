﻿using BLLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manager
{

    public class Controller
    {
        ComplexNumbers complexNumber1, complexNumber2;
        IOperation Calculate = new MathBinaryLogic();
        private string operation;
        private double simpleNumber1, simpleNumber2, calculatorTypeChoose, complexNumber1_real, complexNumber1_imagine, complexNumber2_real, complexNumber2_imagine;
        public void CalculatorChoiseMenu()
        {
            Console.WriteLine("Choose the type of calculator which you'll use: ");
            Console.WriteLine("1.Calculator with simple numbers.");
            Console.WriteLine("2.Calculator with complex numbers.");
            calculatorTypeChoose = CorrectInputNumbers();
        }
        public void SwitchBetweenCalcs()
        {
            bool WrongSwitch=true;
            do
            {
                CalculatorChoiseMenu();
                if (calculatorTypeChoose == 1)
                {
                    InputSimpleNumbers();
                    CalculateSimpleOperands();
                    WrongSwitch = false;
                }
                else if (calculatorTypeChoose == 2)
                {
                    InputComplexNumbers();
                    CalculateComplexOperands();
                    WrongSwitch = false;
                }
                else
                {
                    Console.WriteLine("Incorrect choise.Try again!");
                    WrongSwitch = true;
                }
            } while (WrongSwitch == true);
        }
        public void EndlessCalc()
        {
            string reUseCalc;
            bool endless = false;
            do
            {
                Console.Write("Do you want to use Calculator again?(y/n):");
                reUseCalc = Console.ReadLine().ToLower();
                if (reUseCalc == "y")
                {
                    endless = false;
                    Console.WriteLine();
                    SwitchBetweenCalcs();
                }
                else
                {
                    Console.WriteLine("Have a good day, sir.");
                    endless = false;
                }
            } while (endless == true);

        }
        public void InputSimpleNumbers()
        {
            Console.WriteLine();
            Console.WriteLine("SimCalculate");
            simpleNumber1 = CorrectInputNumbers();
            operation = ValidSignOperation();
            Console.Write("Input the second operand: ");
            simpleNumber2 = CorrectInputNumbers();
        }
        public void InputComplexNumbers()
        {
            Console.WriteLine();
            Console.WriteLine("ComCalculate");
            Console.WriteLine("Input the real and imagine part of first complex number: ");
            Console.Write("Real: ");
            complexNumber1_real = CorrectInputNumbers();
            Console.Write("Imagine: ");
            complexNumber1_imagine = CorrectInputNumbers();
            operation = ValidSignOperation();
            Console.WriteLine("Input the real and imagine part of second complex number: ");
            Console.Write("Real: ");
            complexNumber2_real = CorrectInputNumbers();
            Console.Write("Imagine: ");
            complexNumber2_imagine = CorrectInputNumbers();
            complexNumber1 = new ComplexNumbers(complexNumber1_real, complexNumber1_imagine);
            complexNumber2 = new ComplexNumbers(complexNumber2_real, complexNumber2_imagine);
        }
        public void CalculateSimpleOperands()
        {
            switch (operation)
            {
                case "+":
                    Console.WriteLine($"Your result is: {Calculate.Add(simpleNumber1, simpleNumber2)}"); 
                    break;
                case "-":
                    Console.WriteLine($"Your result is: {Calculate.Substruct(simpleNumber1, simpleNumber2)}");
                    break;
                case "*":
                    Console.WriteLine($"Your result is: {Calculate.Multiply(simpleNumber1, simpleNumber2)}");
                    break;
                case "/":
                    Console.WriteLine($"Your result is: {Calculate.Divide(simpleNumber1, simpleNumber2)}");
                    break;
       
            }
            EndlessCalc();
        }
        public void CalculateComplexOperands()
        {
            switch (operation)
            {
                case "+":
                    Console.WriteLine($"Your result is: {Calculate.Add(complexNumber1, complexNumber2)}");
                    break;
                case "-":
                    Console.WriteLine($"Your result is: {Calculate.Add(complexNumber1, complexNumber2)}");
                    break;
                case "*":
                    Console.WriteLine($"Your result is: {Calculate.Add(complexNumber1, complexNumber2)}");
                    break;
                case "/":
                    Console.WriteLine($"Your result is: {Calculate.Add(complexNumber1, complexNumber2)}");
                    break;
            }
            EndlessCalc();
        }
        public string ValidSignOperation()
        {
            bool correctSymbolInput;
            do
            {
                    Console.Write("Input the operation( / , * , - , + ) operand: ");
                    operation = Console.ReadLine();
                    if ((operation == "+" || operation == "-" || operation == "*" || operation == "/"))
                    {
                        correctSymbolInput = true;
                        return operation;
                    }
                    else
                    {
                        Console.WriteLine("You are trying to input invalid symbol, please choose correct symbol.");
                        correctSymbolInput = false;
                    }
            } while (correctSymbolInput == false);
            return "";
        }
        public double CorrectInputNumbers()
        {
            bool correctInput;
            double temp;
            do
            {
                try
                {
                    Console.Write("Input the number: ");
                    temp = double.Parse(Console.ReadLine());
                    correctInput = true;
                    return temp;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                    correctInput = false;
                }
            } while (correctInput == false);
            return 0;
        }
    }
}