﻿using PokemonCreator;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ListManager
{
   [Serializable]
    public class ListControll
    {
        public delegate void PokemonEventsHandler(object sender, PokemonEventArgs e);
        public event PokemonEventsHandler BeforeAdd;
        public event PokemonEventsHandler AfterAdded;
        public event PokemonEventsHandler MessageHandler;
        private List<AbstractPokemon> PokemonColony = new List<AbstractPokemon>();
        [NonSerialized]
        int iterator = 1;
        public void AddPokemon(AbstractPokemon somePokemon)
        {
            if (AfterAdded != null && BeforeAdd !=null)
            {
                BeforeAdd(this, new PokemonEventArgs("Cheking for the free place in the collection..."));
                PokemonColony.Add(somePokemon);
                AfterAdded(this, new PokemonEventArgs("Pokemon was sucefully added"));
            }
        }
        public void RemovePokemon(int index)
        {
            if (PokemonColony.Count == 0)
            {
                MessageHandler(this,new PokemonEventArgs("Colony is empty."));
            }
            else
            {
                PokemonColony.RemoveAt(index - 1);
                iterator = 1;
            }
        }
        public void ShowPokemon()
        {
            if (PokemonColony.Count == 0)
            {
                MessageHandler?.Invoke(this, new PokemonEventArgs("Colony is empty."));
            }
            else
            {
                foreach (var item in PokemonColony)
                {
                    MessageHandler?.Invoke(this, new PokemonEventArgs($"Pokemon number:{iterator}\n{item}"));
                    iterator++;
                    Console.WriteLine();
                }
            }
           
        }
        public void SortPokemon()
        {
            if (PokemonColony.Count == 0)
            {
                MessageHandler?.Invoke(this, new PokemonEventArgs("Colony is empty."));
            }
            else
            {
                PokemonColony.Sort((p1, p2) => p1.Name.CompareTo(p2.Name));
            }
        }
        public void Serialize()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<AbstractPokemon>));
            using (FileStream fs = new FileStream("Pokemons.xml", FileMode.Create))
            {
                formatter.Serialize(fs, this.PokemonColony);
                MessageHandler?.Invoke(this, new PokemonEventArgs("Succesfully saved!"));
            }
        }
        public void Deserialize()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<AbstractPokemon>));
            using (FileStream fs = new FileStream("Pokemons.xml", FileMode.OpenOrCreate))
            {
                PokemonColony = (List<AbstractPokemon>)formatter.Deserialize(fs);
            }
            MessageHandler?.Invoke(this, new PokemonEventArgs("File was loaded,check your collection"));
        }
        
    }
}
