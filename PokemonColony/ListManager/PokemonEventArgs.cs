﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListManager
{
   public class PokemonEventArgs:EventArgs
    {
        public readonly string Message;
        public PokemonEventArgs(string msg)
        {
            Message = msg;
        }
    }
}
