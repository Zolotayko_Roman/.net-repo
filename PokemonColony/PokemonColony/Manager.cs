﻿using ListManager;
using PokemonCreator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonColony
{
    class Manager
    {
        private ListControll NewPokemonColony = new ListControll();
        public Manager()
        {
            NewPokemonColony.BeforeAdd += EventMessageHandler;
            NewPokemonColony.AfterAdded += EventMessageHandler;
            NewPokemonColony.MessageHandler += EventMessageHandler;
        }
        private int pokemonTypeChoose, NewPokemonLevel,mainMenuChoise;
        private string NewPokemonName, NewPokemonAbility,EndlessChoose;
        private bool EndlessPokemon;
        public void PokemonAddMenu()
        {
            Console.WriteLine("You can add next types of pokemons: ");
            Console.WriteLine("1.Water Pokemon\n2.Wind Pokemon\n3.Shadow Pokemon\n4.Electro Pokemon");
            Console.Write("Which type you'll choose?:");
            pokemonTypeChoose = CorrectInputNumbers();
            Console.Write("How you will call your new pokemon?:");
            NewPokemonName = Console.ReadLine();
            Console.Write("What ability will he have?:");
            NewPokemonAbility = Console.ReadLine();
            Console.Write("Assign level to your pokemon,");
            NewPokemonLevel = CorrectInputNumbers();
        }

        public void AddNewPokemon()
        {
            PokemonAddMenu();
            switch (pokemonTypeChoose)
            {
                case 1:
                    NewPokemonColony.AddPokemon(new WaterPokemon(NewPokemonName,NewPokemonAbility,NewPokemonLevel));
                    break;
                case 2:
                    NewPokemonColony.AddPokemon(new WindPokemon(NewPokemonName, NewPokemonAbility, NewPokemonLevel));
                    break;
                case 3:
                    NewPokemonColony.AddPokemon(new ShadowPokemon(NewPokemonName, NewPokemonAbility, NewPokemonLevel));
                    break;
                case 4:
                    NewPokemonColony.AddPokemon(new ElectroPokemon(NewPokemonName, NewPokemonAbility, NewPokemonLevel));
                    break;
                
            }
        }
        public void MainMenu()
        {
            Console.WriteLine();
            Console.WriteLine("=====NewPokemonHell=====");
            Console.WriteLine("Avaliable next operations:");
            Console.WriteLine("1.Add new pokemon\n2.Remove pokemon(Hint:index)\n3.Show pokemon list\n4.Sort Pokemons(ByName)\n5.Save progress\n6.Load last save");
            Console.Write("What you will choose?,");
            mainMenuChoise = CorrectInputNumbers();

        }
        public void MainMenuSwitch()
        {
            do
            {
                MainMenu();
                switch (mainMenuChoise)
                {
                    case 1:
                        AddNewPokemon();
                        break;
                    case 2:
                        RemoveAtIndexPokemon();
                        break;
                    case 3:
                        ShowPokemonList();
                        break;
                    case 4:
                        SortPokemonList();
                        break;
                    case 5:
                        NewPokemonColony.Serialize();
                        break;
                    case 6:
                        NewPokemonColony.Deserialize();
                        break;
                }
                EndlessPokemonCycle();
            } while (EndlessPokemon == true);
        }
        public int CorrectInputNumbers()
        {
            bool correctInput;
            int temp;
            do
            {
                try
                {
                    Console.Write("enter your numerals: ");
                    temp = int.Parse(Console.ReadLine());
                    correctInput = true;
                    return temp;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine($"Error:{ex.Message}");
                    correctInput = false;
                }
            } while (correctInput == false);
            return 0;
        }
        public void RemoveAtIndexPokemon()
        {
            int indexRemove;
            Console.Write("Which pokemon do you want to remove from list?: ");
            indexRemove = CorrectInputNumbers();
            NewPokemonColony.RemovePokemon(indexRemove);
        }
        public void ShowPokemonList()
        {
            NewPokemonColony.ShowPokemon();
        }
        public void SortPokemonList()
        {
            NewPokemonColony.SortPokemon();
        }
        public void EndlessPokemonCycle()
        {
            EndlessPokemon = false;
            do
            {
                Console.Write("You wanna play in <this> again?)(yes||no):");
                EndlessChoose = Console.ReadLine().ToLower();
                if (EndlessChoose == "yes")
                {
                    EndlessPokemon = true;
                }
                else if (EndlessChoose == "no")
                {
                    EndlessPokemon = false;
                }
                else
                {
                    Console.WriteLine("I think you choosed something wrong. You can try again.");
                }
            } while (EndlessPokemon == false);
        }
        public void EventMessageHandler(object sender, PokemonEventArgs e)
        {
            Console.WriteLine(e.Message);
        }
    }
}
