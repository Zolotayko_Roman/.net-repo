﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PokemonCreator
{
    [Serializable]
    [XmlInclude (typeof(WaterPokemon))]
    [XmlInclude(typeof(WindPokemon))]
    [XmlInclude(typeof(ElectroPokemon))]
    [XmlInclude(typeof(ShadowPokemon))]
    abstract public class AbstractPokemon
    {
        public int DamageSkill { get; set; }
        public string SkillType { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public PokemonTypes.PokemonType Type {get;set;}
        public string Ability {get;set;}
        public int Attack { get; set; }
        public int Defence { get; set; }
        public AbstractPokemon()
        {

        }
        public AbstractPokemon(string name, string ability, int level)
        {
            Name = name;
            Level = level;
            Ability = ability;
        }
        public override string ToString()
        {
            
            return string.Format($"Pokemon name: {Name}\nPokemon level: {Level}\nPokemon type: {Type}\nPokemon attack: {Attack}," +
                $"Pokemon defence: {Defence}\nPokemon Ability:{Ability}");
        }
    }
}
