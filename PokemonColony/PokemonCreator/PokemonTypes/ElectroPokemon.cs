﻿using System;

namespace PokemonCreator
{
    [Serializable]
    public class ElectroPokemon : AbstractPokemon
    {
        public ElectroPokemon()
        {

        }
        public ElectroPokemon(string name, string ability, int level) : base(name, ability, level)
        {
            Attack = 3 * level;
            Defence = 2 * level;
            Type = PokemonTypes.PokemonType.Electro;
        }
    }
}
