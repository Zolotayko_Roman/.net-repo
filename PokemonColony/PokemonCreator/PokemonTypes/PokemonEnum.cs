﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonCreator.PokemonTypes
{
    public enum PokemonType
    {
        Electro=1,
        Water,
        Wind,
        Shadow
    }
}
