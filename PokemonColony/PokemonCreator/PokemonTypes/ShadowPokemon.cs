﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonCreator
{
    [Serializable]
    public class ShadowPokemon : AbstractPokemon
    {
        public ShadowPokemon()
        {

        }
        public ShadowPokemon(string name, string ability, int level) : base(name, ability, level)
        {
            Attack = 4 * level;
            Defence = 2 * level;
            Type = PokemonTypes.PokemonType.Shadow;
        }

    }
}
