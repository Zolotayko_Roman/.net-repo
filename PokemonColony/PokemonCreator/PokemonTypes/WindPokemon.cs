﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonCreator
{
    [Serializable]
    public class WindPokemon : AbstractPokemon
    {
        public WindPokemon()
        {

        }
        public WindPokemon(string name, string ability, int level) : base(name, ability, level)
        {
            Attack = 5 * level;
            Defence = 3 * level;
            Type = PokemonTypes.PokemonType.Wind;
        }

    }
}
